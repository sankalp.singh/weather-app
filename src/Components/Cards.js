import React, { Component } from "react";
import "./Cards.css";

class Cards extends Component {
	// constructor(props) {
	// 	super(props);
	// 	this.state = {
	// 		details: "",
	// 		days: "",
	// 	};
	// }
	// getDay() {}

	// getWeatherData() {
	// 	this.setState({
	// 		details: this.props.weatherData,
	// 		days: this.props.dayNames,
	// 	});
	// }
	getDayName(dayNo) {
		if (dayNo === 0) {
			return "Sun";
		} else if (dayNo === 1) {
			return "Mon";
		} else if (dayNo === 2) {
			return "Tue";
		} else if (dayNo === 3) {
			return "Wed";
		} else if (dayNo === 4) {
			return "Thu";
		} else if (dayNo === 5) {
			return "Fri";
		} else {
			return "Sat";
		}
	}

	render() {
		console.log(this.props.weatherData);
		// console.log(this.props.weatherData);
		// console.log(this.props.weatherData);
		// console.log(this.props.dayNames);
		// let details = this.props.weatherData;
		// let days = this.props.dayNames;
		console.log("Inside cards render");

		return (
			<div className="cardList">
				{this.props.weatherData.length !== 0 &&
					this.props.weatherData.map((obj) => {
						return (
							<div className="cards">
								<h5>{this.getDayName(obj.day)}</h5>
								<img
									src={`http://openweathermap.org/img/w/${obj.details.weather[0].icon}.png`}
									alt="weather icon"
								/>
								<p>{obj.details.main.temp_min}°C</p>
								<p>{obj.details.main.temp_max}°C</p>
							</div>
						);
					})}

				{/* <div className="cards">
					<img src={iconUrl} alt="weather icon" />
					<p>Min Temp: 28 C</p>
					<p>Max Temp : 36 C</p>
				</div>
				<div className="cards">
					<img src={iconUrl} alt="weather icon" />
					<p>Min Temp: 28 C</p>
					<p>Max Temp : 36 C</p>
				</div>
				<div className="cards">
					<img src={iconUrl} alt="weather icon" />
					<p>Min Temp: 28 C</p>
					<p>Max Temp : 36 C</p>
				</div>
				<div className="cards">
					<img src={iconUrl} alt="weather icon" />
					<p>Min Temp: 28 C</p>
					<p>Max Temp : 36 C</p>
				</div>
				<div className="cards">
					<img src={iconUrl} alt="weather icon" />
					<p>Min Temp: 28 C</p>
					<p>Max Temp : 36 C</p>
				</div> */}
			</div>
		);
	}
}
export default Cards;
