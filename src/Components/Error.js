import React, { Component } from "react";
import "./Error.css";

class Error extends Component {
	render() {
		console.log("Inside error component");
		return (
			<div className="error-bar">
				<h3>Invalid Input</h3>
				<p>Error : 404</p>
			</div>
		);
	}
}
export default Error;
