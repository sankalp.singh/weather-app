import React, { Component } from "react";
import "./Search.css";

class search extends Component {
	constructor(props) {
		super(props);
		this.state = {
			searchData: "",
		};
	}

	inputDataHandler = (e) => {
		this.setState({
			searchData: e.target.value,
		});
	};
	submitHandler = (e) => {
		this.props.inputCity.getCity(this.state.searchData);
	};

	render() {
		return (
			<div className="input-area">
				<input
					type="text"
					name="city"
					placeholder=" Enter City"
					onChange={this.inputDataHandler}
				/>
				<button onClick={this.submitHandler}>Get Data</button>
			</div>
		);
	}
}
export default search;
