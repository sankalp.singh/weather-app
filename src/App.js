import React, { Component } from "react";
import Cards from "./Components/Cards";
import Search from "./Components/Search";
import Error from "./Components/Error";
import axios from "axios";
import "./App.css";

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			city: "",
			info: [],
			fetchedDataFlag: false,
			msg: "",
		};
	}
	getCity(name) {
		this.setState({ city: name });
		console.log("state");
		this.fetchData(name);
	}

	fetchData = (name) => {
		axios
			.get(
				`https://api.openweathermap.org/data/2.5/forecast?q=${name}&units=metric&appid=033384ba48fcf458d8c30002f0eb8fc7`
			)
			.then((data) => {
				console.log(data.data);
				this.setState({
					fetchedDataFlag: true,
					msg: data.data.message,
				});

				if (data.data.message === 0) {
					let weatherArr = data.data.list;
					let finalReport = this.reportGenerator(weatherArr);
					this.setState({
						info: finalReport,
					});
				} else {
					this.setState({
						msg: data.data.message,
					});
				}
			})
			.catch((err) => {
				// console.error(err);
				this.setState({
					msg: "City not Found",
				});
			});
	};
	reportGenerator(weatherArr) {
		let dayNames = weatherArr.map((day) => {
			let currDate = new Date(day["dt_txt"]);
			return currDate.getDay();
		});
		dayNames = [...new Set(dayNames)];
		let report = dayNames.map((day) => {
			let filterData = weatherArr.filter((item) => {
				let curr = new Date(item["dt_txt"]);
				let currDay = curr.getDay();
				if (day === currDay) {
					return true;
				}
				return false;
			});
			let final = {
				day: day,
				details: filterData[0],
			};
			return final;
		});
		return report;
	}
	render() {
		// let displayCard = ``;
		// if (this.state.fetchedDataFlag === true) {
		// 	displayCard = `<Cards weatherData={this.weather} />`;
		// }
		return (
			<div className="App">
				<h1>Weather App</h1>
				<Search
					inputCity={{
						city: this.state.city,
						getCity: this.getCity.bind(this),
					}}
				/>
				{/* {this.state.msg !== "0" && <Error message={this.state.msg} />}
				{this.state.fetchedDataFlag && <Cards weatherData={this.state.info} />} */}
				{console.log(this.state.msg)}
				{this.state.msg === undefined || this.state.msg === `City not Found` ? (
					<Error message={this.state.msg} />
				) : this.state.fetchedDataFlag ? (
					<Cards weatherData={this.state.info} />
				) : null}
			</div>
		);
	}
}
export default App;
